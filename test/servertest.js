const ourserver = require("../server");
const supertest = require("supertest");

describe('Our server', function () {
  it('should send back a 200 code', function (done) {
    supertest(ourserver)
      .get("/retoibm/sumar/100/200")
      .expect(200)
      .end(function (err, res) {
        if (err) {
          done(err);
        }
        done();
      });
  });
  it('should send back a Bad Request', function (done) {
    supertest(ourserver)
      .get("/retoibm/sumar/I/V")
      .expect(400)
      .end(function (err, res) {
        if (err) {
          done(err);
        }
        done();
      });
  });
});