'use strict';

var expect = require("chai").expect;
var calculator = require("../implementation/operation");

describe("Calculator - Test", function () {

  describe("Testing the Operations", function () {

    it("Testing the addition operation", function () {
      expect(calculator.addition(1, 1)).to.equal(1 + 1);
    });

    it("Testing the addition operation for decimal numbers", function () {
      expect(calculator.addition(10.4, 10.5)).to.equal(20.9);
    });

    it("Testing the addition operation using zero", function () {
      expect(calculator.addition(0, 0)).to.equal(0);
    });
  });
});