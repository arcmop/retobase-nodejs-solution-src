const _express = require('express');
const _server = _express();
console.log("Before oS");
const _os = require("os");
console.log("Before oS get hostname");
const _hostName = _os.hostname();
console.log("After oS get hostname:" + _hostName);
console.log("Loading code:" + _hostName);
const _operator = require(__dirname + '/implementation/operation.js');

_server.get('/retoibm/sumar/:sumando01/:sumando02', function (request, response) {
  try {
    var _resultado = _operator.addition(request.params.sumando01, request.params.sumando02);

    if (typeof _resultado !== "undefined" && _resultado !== null && !isNaN(_resultado)) {
      var _result = new Object();
      _result.resultado = _resultado;
      _result.hostname = _hostName;
      _result.language = "javascript";
      return response.send(JSON.stringify(_result));
    } else {
      return response.status(400).json({ resultado: "Bad Request" });
    }
  }
  catch (e) {
    console.log("error" + e);
    return response.status(500).json({ resultado: e });
  }
});

module.exports = _server;