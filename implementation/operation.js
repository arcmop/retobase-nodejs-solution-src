function addition(sumando01, sumando02) {
  try {
    var _sumando01 = new Number(sumando01);
    var _sumando02 = new Number(sumando02);
    return _sumando01 + _sumando02;
  }
  catch (exception) {
    return null;
  }
}

module.exports = { addition };