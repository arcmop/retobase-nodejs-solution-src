const { setWorldConstructor } = require("@cucumber/cucumber");
const _operator = require("../../implementation/operation.js")

class CustomWorld {
  constructor() {
    this.firstOperand = 0;
    this.secondOperand = 0;
    this.result = 0;
  }

  setFirstOperand(number) {
    this.firstOperand = number;
  }

  addTo(operand) {
    this.secondOperand = operand;
    this.result = _operator.addition(this.firstOperand, this.secondOperand)
  }
}

setWorldConstructor(CustomWorld);