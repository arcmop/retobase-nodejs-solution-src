Feature: Addition
  In order to perform numeral arithmetics
  As a developer
  I want to add two roman numbers together
 
  Scenario: basic addition
    Given I have the number "20"
    When I add it to the number "48"
    Then the result should be the number "68"

  Scenario: decimal addition
    Given I have the number "20.45"
    When I add it to the number "30.58"
    Then the result should be the number "51.03"

  Scenario: negative addition
    Given I have the number "-10.50"
    When I add it to the number "-20.10"
    Then the result should be the number "-30.60"