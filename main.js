const _port = 4000;
const _server =  require(__dirname + '/server.js');
_server.listen(_port, () => {
  console.log(`Server listening at ${_port}`);
});